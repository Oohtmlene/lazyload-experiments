const { RESTDataSource } = require('apollo-datasource-rest');

class API extends RESTDataSource {
  constructor() {
    super();
    this.baseURL =
      'https://d7fcde6b-6151-4155-bd48-8a36412cccb6.mock.pstmn.io/';
  }

  async getSections() {
    const { data } = await this.get('project-homepage');
    return data.project_homepage.sections;
  }

  async getWidget({ args }) {
    const { data } = await this.get(`get?widget=${args.uid}`);
    return data.widget;
  }

  async getSchoolBooks({ args }) {
    const { data } = await this.get(`get?books`);
    return data;
  }
}

module.exports = API;

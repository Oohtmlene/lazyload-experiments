const { gql } = require('apollo-server');

const typeDefs = gql`
  type Query {
    sections: [Section]
    widget(uid: ID): Widget
    schoolBooks: [Book]
  }

  interface Book {
    title: String
    author: String
  }

  type Textbook implements Book {
    title: String
    author: String
    courses: [Course]
  }

  type Course {
    title: String
  }

  type Section {
    uid: ID!
    position: Int!
    widgets: [Widget]
  }

  type Widget {
    uid: ID!
    type: String!
    position: Int!
    items: [Item]
  }

  type Item {
    uid: ID!
    position: Int!
    type: Type
    span: Int
    title: String
    description: String
    src: String
    poster: String
    img: Img
  }

  type Img {
    src: String
    dataSrc: String
    dataSrcset: String
    srcset: String
  }

  enum Type {
    video
    image
    playlist
    chart
    document
    searchQuery
  }
`;

module.exports = typeDefs;

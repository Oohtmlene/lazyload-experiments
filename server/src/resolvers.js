const resolvers = {
  Book: {
    __resolveType(book, context, info) {
      console.log('4 resolver', book);
      if (!!book.courses) {
        return 'TextBook';
      }
      return null;
    },
  },
  Query: {
    schoolBooks: async (parent, args, { dataSources }, info) => {
      return dataSources.API.getSchoolBooks({ args });
      // const data = await dataSources.API.getSchoolBooks({ args });
      // if (data.courses) {
      //   return {
      //     __typename: 'Textbook',
      //     ...data,
      //   };
      // }
      // return {
      //   __typename: 'Book',
      //   ...data,
      // };
    },
    sections: (parent, args, { dataSources }, info) => {
      return dataSources.API.getSections();
    },
    widget: (parent, args, { dataSources }, info) => {
      return dataSources.API.getWidget({ args });
    },
  },
};

module.exports = resolvers;

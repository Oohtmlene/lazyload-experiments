const { ApolloServer /* MockList */ } = require('apollo-server');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const API = require('./datasources/api');

const server = new ApolloServer({
  // enable basic mocked data, instructs Apollo Server to populate every queried schema field with a placeholder value:
  // mocks: true,
  // enable more realistic mocked data:
  // mocks,
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      API: new API(),
    };
  },
});

// start server up
server.listen().then(() => {
  console.log(`
    🚀  Server is running!
    🔉  Listening on port 4000
    📭  Query at https://studio.apollographql.com/dev
`);
});

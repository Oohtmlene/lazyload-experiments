import sections from './sections_loads.json';
import files from './files';
import faker from 'faker';

const buildData = () => {
  const widgets = sections.data.children[0].children;
  const fullData = widgets.map((widget, i) => {
    const cards = widget.children.map((card, j) => {
      return {
        ...card,
        title: faker.lorem.sentence(),
        description: faker.lorem.paragraph(),
        poster: files[i][j].img,
        src: card.cat === 'video' ? files[i][j].video : files[i][j].img,
      };
    });

    return {
      ...widget,
      children: [...cards],
    };
  });

  console.log('fullData', fullData);

  return 'data';
};

export default buildData;

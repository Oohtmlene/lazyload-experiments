import React from 'react';

const PreviewContext = React.createContext();
PreviewContext.displayName = 'Preview';

function PreviewProvider(props) {
  const [preview, setPreview] = React.useState({
    show: false,
    trigger: null,
    component: null,
    data: null,
  });

  const value = [preview, setPreview];

  return <PreviewContext.Provider value={value} {...props} />;
}

function usePreview() {
  const context = React.useContext(PreviewContext);
  if (context === undefined) {
    throw new Error('usePreview must be used within a <PreviewProvider />');
  }
  return context;
}

export { PreviewProvider, usePreview };

import React from 'react';

// Will need to come from BE
const ModeContext = React.createContext();
ModeContext.displayName = 'ModeContext';

function ModeProvider(props) {
  const [mode, setMode] = React.useState('view');
  const value = [mode, setMode];
  return <ModeContext.Provider value={value} {...props} />;
}

function useMode() {
  const context = React.useContext(ModeContext);
  if (context === undefined) {
    throw new Error('useMode must be used within a <ModeProvider />');
  }
  return context;
}

export { ModeProvider, useMode };

import React, { Fragment } from 'react';
import { Router } from '@reach/router';
import Home from './Home';
import Home2 from './Home2';

export default function Routes() {
  return (
    <Router primary={false} component={Fragment}>
      <Home path="/" />
      <Home2 path="/2" />
    </Router>
  );
}

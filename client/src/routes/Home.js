import React, { Fragment } from 'react';
import { element, shape, string, arrayOf } from 'prop-types';
import styled from 'styled-components';
import { useQuery, gql } from '@apollo/client';
import GlobalStyles from '../globalStyles';
import { PreviewProvider } from '../contexts/PreviewContext';
import { ModeProvider, useMode } from '../contexts/ModeContext';
import PreviewHolder from '../components/PreviewHolder';
import Cards from '../components/Cards';

export const SECTIONS = gql`
  query getSections {
    sections {
      uid
      widgets {
        uid
        type
        columns
      }
    }
  }
`;

// SECTION
const Section = ({ children }) => {
  return <section>{children}</section>;
};

Section.propTypes = {
  children: arrayOf(element),
};

Section.defaultProps = {
  children: [],
};

// INNER
const SInner = styled.div`
  margin: 0 2.5vw;
`;

const WidgetInner = ({ children }) => {
  return <SInner>{children}</SInner>;
};

WidgetInner.propTypes = {
  children: element.isRequired,
};

// WIDGET
const Widget = ({ widget }) => {
  const items = [{
          "uid": "card1",
          "type": "video",
          "title": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
          "description":
            "Lorem ipsum dolor sit amet, magna in neque. Consectetur adipiscing elit Vivamus nec magna in.",
          "src":
            "https://assets.mixkit.co/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-small.mp4",
          "poster":
            "https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
          "img": {
            "src":
              "https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
            "data-src":
              "https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
            "data-srcset":
              "https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=80&amp;dpr=1 1x, https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=50&amp;dpr=2 2x, https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=30&amp;dpr=3 3x, https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=15&amp;dpr=4 4x",
            "srcset":
              "https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=80&amp;dpr=1 1x,https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=50&amp;dpr=2 2x,https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=30&amp;dpr=3 3x,https://mixkit.imgix.net/videos/preview/mixkit-fireworks-illuminating-the-beach-sky-4157-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=15&amp;dpr=4 4x",
          },
        },    
        {
          "uid": "card2",
          "type": "video",
          "title": "Vivamus nec magna in neque feugiat feugiat semper non libero.",
          "description":
            "Aenean et tortor vitae quam ultricies rhoncus uid in purus. ",
          "src":
            "https://assets.mixkit.co/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-small.mp4",
          "poster":
            "https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
          "img": {
            "src":
              "https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
            "data-src":
              "https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
            "data-srcset":
              "https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=80&amp;dpr=1 1x, https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=50&amp;dpr=2 2x, https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=30&amp;dpr=3 3x, https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=15&amp;dpr=4 4x",
            "srcset":
              "https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=80&amp;dpr=1 1x,https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=50&amp;dpr=2 2x,https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=30&amp;dpr=3 3x,https://mixkit.imgix.net/videos/preview/mixkit-woman-walking-on-beach-towards-boulders-1012-0.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=15&amp;dpr=4 4x",
          },
        },
        {
          "uid": "card3",
          "type": "video",
          "title": "Lorem Vivamus ipsum dolor sit amet, consectetur adipiscing elit.",
          "description":
            "consectetur adipiscing elit Vivamus nec magna in neque feugiat elit feugiat semper non libero non libero. ",
          "src":
            "https://assets.mixkit.co/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-small.mp4",
          "poster":
            "https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
          "img": {
            "src":
              "https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
            "data-src":
              "https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress",
            "data-srcset":
              "https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=80&amp;dpr=1 1x, https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=50&amp;dpr=2 2x, https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=30&amp;dpr=3 3x, https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=15&amp;dpr=4 4x",
            "srcset":
              "https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=80&amp;dpr=1 1x,https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=50&amp;dpr=2 2x,https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=30&amp;dpr=3 3x,https://mixkit.imgix.net/videos/preview/mixkit-rain-falling-on-the-water-of-a-lake-seen-up-18312-1.jpg?w=460&amp;q=80&amp;auto=format%2Ccompress&amp;q=15&amp;dpr=4 4x",
          }
        }
      ]
    

  // getting data here?
  /**
   * fetch /widget/uid
   * return widget: {
   *    "uid": "widget1"
   *    type: "card",
   *    items: []
   * }
   */

  switch (widget.type) {
    case 'cards':
      return (
        <WidgetInner>
          <Cards uid={widget.uid}>
            {widget.columns.map((_, i) => {
              const cardProps = {
                widgetUid: widget.uid,
                index: i,
                span: widget.columns[i],
                card: items[i],
              };
              return (
                <Fragment key={`card-${i}`}>
                  <Cards.CardView {...cardProps} />
                  <Cards.CardEdit {...cardProps} />
                </Fragment>
              );
            })}
          </Cards>
        </WidgetInner>
      );
    case 'hero':
      return <h3>Hero component</h3>;
    case 'headline':
      return (
        <WidgetInner>
          <h3>Headline component</h3>
        </WidgetInner>
      );
    default:
      return 'No Widget for this type';
  }
};

Widget.propTypes = {
  widget: shape({
    uid: string.isRequired,
    type: string.isRequired,
  }),
};

const HomePageMode = () => {
  const [mode, setMode] = useMode();
  const handleChangeMode = () => setMode(mode === 'view' ? 'edit' : 'view');

  return (
    <h2>
      Mode: {mode} <button onClick={handleChangeMode}>Change mode</button>
    </h2>
  );
};

const Home = () => {
  const { loading, error, data } = useQuery(SECTIONS);

  if (loading) return 'loading';
  console.log(error);

  const { sections } = data;
  return (
    <>
      <GlobalStyles />
      <ModeProvider>
        <PreviewProvider>
          <div
            style={{
              position: 'fixed',
              top: 0,
              left: 0,
              width: '90px',
              height: '100vh',
              background: '#374249',
            }}
          />
          <h1>Project homepage</h1>
          <HomePageMode />
          {sections.map((section) => (
            <Section key={section.uid}>
              {section.widgets.map((widget) => (
                <Widget key={widget.uid} widget={widget}></Widget>
              ))}
            </Section>
          ))}
          <PreviewHolder />
        </PreviewProvider>
      </ModeProvider>
    </>
  );
};

export default Home;

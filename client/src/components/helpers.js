import React from 'react';
import PlaylistPlay from '@material-ui/icons/PlaylistPlay';
import OndemandVideo from '@material-ui/icons/OndemandVideo';
import ImageOutlined from '@material-ui/icons/ImageOutlined';
import DescriptionOutlined from '@material-ui/icons/DescriptionOutlined';
import TimelineOutlined from '@material-ui/icons/TimelineOutlined';
import FindInPageOutlined from '@material-ui/icons/FindInPageOutlined';
import RemoveOutlined from '@material-ui/icons/RemoveOutlined';

const getAvatar = (type) => {
  switch (type) {
    case 'video':
      return <OndemandVideo />;
    case 'playlist':
      return <PlaylistPlay />;
    case 'image':
      return <ImageOutlined />;
    case 'document':
      return <DescriptionOutlined />;
    case 'chart':
      return <TimelineOutlined />;
    case 'search query':
      return <FindInPageOutlined />;
    default:
      return <RemoveOutlined />;
  }   
}



export { getAvatar };

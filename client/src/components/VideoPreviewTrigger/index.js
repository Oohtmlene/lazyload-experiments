import React from 'react';
import { oneOf, element, shape, string, node } from 'prop-types';
import styled from 'styled-components';
import { usePreview } from '../../contexts/PreviewContext';

function PreviewTrigger({ children, event, component, data }) {
  const [, setPreview] = usePreview();
  const [delayHandler, setDelayHandler] = React.useState(null);

  const handleClick = (e) => {
    e.persist();
    setPreview((prevPreview) => ({
      ...prevPreview,
      show: true,
      trigger: e,
      component,
      data,
    }));
  };

  const handleMouseEnter = (e) => {
    e.persist();
    setDelayHandler(
      setTimeout(() => {
        setPreview((prevPreview) => ({
          ...prevPreview,
          show: true,
          trigger: e,
          component,
          data,
        }));
      }, 250)
    );
  };

  const handleMouseLeave = () => {
    clearTimeout(delayHandler);
  };

  const events = {
    click: { onClick: handleClick },
    hover: { onMouseEnter: (e) => handleMouseEnter(e) },
  };

  return (
    <SPreviewTrigger
      {...(event === 'mouseenter' ? { onMouseLeave: handleMouseLeave } : {})}
      {...events[event]}
    >
      {children}
    </SPreviewTrigger>
  );
}

PreviewTrigger.propTypes = {
  children: node.isRequired,
  component: element.isRequired,
  data: shape({
    src: string,
  }),
  event: oneOf(['click', 'hover', 'none']),
};

PreviewTrigger.defaultProps = {
  event: 'none',
  data: shape({
    src: '',
  }),
};

const SPreviewTrigger = styled.div`
  // pointer-events: none;
  opacity: 1;
  transition: opacity 250ms ease-out;
  background: black;
  &:hover {
    /* opacity: 0.95; */
  }
`;

export default PreviewTrigger;

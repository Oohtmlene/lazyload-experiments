import React from 'react';
import { string, shape, node, element, number } from 'prop-types';
import styled from 'styled-components';
import { useMode } from '../../contexts/ModeContext';
import Cards from '../Cards2';

const WidgetWrapper = ({ id, position, children }) => {
  const [mode] = useMode();
  const isEditMode = mode === 'edit';

  const handleDelete = () => console.log('delete', id, position);
  const handleSelect = () => console.log('select', id, position);
  const handleMoveUp = () => console.log('Move up', id, position);
  const handleMoveDown = () => console.log('Move down', id, position);

  return (
    <div>
      {isEditMode && (
        <div style={{ display: 'flex' }}>
          <button onClick={handleDelete}>Delete</button>
          <button onClick={handleSelect}>Select</button>
          <button onClick={handleMoveUp}>Move up</button>
          <button onClick={handleMoveDown}>Move down</button>
        </div>
      )}
      <div style={{ marginTop: '8px' }}>{children}</div>
    </div>
  );
};

WidgetWrapper.propTypes = {
  children: node.isRequired,
  id: string.isRequired,
  position: number.isRequired,
};

const Widget = ({ widget }) => {
  const { id, position } = widget;
  switch (widget.type) {
    case 'cards':
      return (
        <WidgetInner>
          <WidgetWrapper id={id} position={position}>
            <Cards widget={widget} />
          </WidgetWrapper>
        </WidgetInner>
      );
    case 'hero':
      return <h2>Hero</h2>;
    case 'headline':
      return (
        <WidgetInner>
          <h2>Headline</h2>
        </WidgetInner>
      );
    default:
      return 'No Widget for this type';
  }
};

Widget.propTypes = {
  widget: shape({
    uid: string.isRequired,
    type: string.isRequired,
  }),
};

export default Widget;

const SInner = styled.div`
  margin: 0 2.5vw;
`;

const WidgetInner = ({ children }) => {
  return <SInner>{children}</SInner>;
};

WidgetInner.propTypes = {
  children: element.isRequired,
};

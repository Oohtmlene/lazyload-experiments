

import React from 'react';
import { element, arrayOf } from 'prop-types';

const Section = ({ children }) => {
  return <section>{children}</section>;
};

Section.propTypes = {
  children: arrayOf(element),
};

Section.defaultProps = {
  children: [],
};

export default Section;

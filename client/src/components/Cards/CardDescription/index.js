import React from 'react';
import { oneOf, element } from 'prop-types';
import styled, { css } from 'styled-components';

// CARD DESCRIPTION
const CardDescription = ({ children, size }) => {
  return <SCardDescription size={size}>{children}</SCardDescription>;
};

// eslint-disable-next-line no-unused-vars
const SCardDescription = styled(({ size, ...rest }) => <div {...rest} />)`
  position: relative;
  z-index: 3;
  ${({ size }) => {
    if (size !== 'sm') {
      return css`
        float: left;
        width: calc(35% - 12px);
        margin-right: 12px;
      `;
    }
  }}
`;

CardDescription.propTypes = {
  children: element.isRequired,
  size: oneOf(['sm', 'md', 'lg']),
};

CardDescription.defaultProps = {
  size: 'sm',
};

export default CardDescription;

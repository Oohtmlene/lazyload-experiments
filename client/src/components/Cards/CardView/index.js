import React from 'react';
import { shape, string, oneOf, number } from 'prop-types';
import { CardHeader, CardContent, Avatar, Grid } from '@material-ui/core';
import VideoPreview from '../../VideoPreview';
import CardDescription from '../CardDescription';
import CardThumbnail from '../CardThumbnail';
import { getAvatar } from '../../helpers';
import { SCard } from '../styles';

const CardView = ({ card }) => {
  const { title = "title", description = 'Description', span } = card;
  // fetch data here or in card?
  const size = {
    xs: 12,
    // sm: 6,
    md: span,
    lg: span,
    xl: span,
  };

  console.log('21', span, size);

  const getRatio = () => {
    switch (span) {
      case 12:
        return 'lg';
      case 8:
        return 'md';
      default:
        return 'sm';
    }
  };

  const renderCardContent = () => {
    return <CardContent>{description}</CardContent>;
  };

  const renderThumbnail = () => {
    switch (card.type) {
      case 'video':
        return <VideoPreview item={card} />;
      case 'playlist':
        return <VideoPreview item={card} />;
      case 'image':
        return <img src={card.img.src} alt={card.title} width='100%' height="275" style={{ display: "block" }} />;
      case 'document':
        return <>Placeholder Document</>;
      case 'chart':
        return <>Placeholder Chart</>;
      case 'search query':
        return <>Placeholder SearchQuery</>;
      default:
        return <>Thumbnail</>;
    }
  };

  return (
    <Grid item component="li" {...size}>
      <SCard>
        <CardHeader
          avatar={
            <Avatar aria-label="type">
              {getAvatar(card.type)}
            </Avatar>
          }
          title={title}
          titleTypographyProps={{
            variant: 'subtitle2',
          }}
        />
        <CardThumbnail size={getRatio()}>{renderThumbnail()}</CardThumbnail>
        <CardDescription size={getRatio()}>
          {renderCardContent()}
        </CardDescription>
      </SCard>
    </Grid>
  );
};

CardView.propTypes = {
  card: shape({
    span: number,
    type: oneOf([
      'image',
      'video',
      'document',
      'playlist',
      'chart',
      'placeholder',
    ]),
    title: string,
    description: string,
  }),
};

CardView.defaultProps = {
  card: {
    span: 12,
    type: 'placeholder',
    title: 'Title',
    description: 'Description',

  },
};

export default CardView;

import React from 'react';
import { shape, string, oneOf, number } from 'prop-types';
import { CardHeader, CardContent, Avatar, Grid } from '@material-ui/core';
import CardThumbnail from '../CardThumbnail';
import CardDescription from '../CardDescription';
import { getAvatar } from '../../helpers';
import { SCard } from '../styles';

const CardEdit = ({ card }) => {
  const { title = "title", description = 'Description', span } = card;

  const renderCardContent = () => {
    return <CardContent>{description}</CardContent>;
  };

  const size = {
    xs: 12,
    // sm: span,
    md: span,
    lg: span,
    xl: span,
  };

  const getRatio = () => {
    switch (span) {
      case 12:
        return 'lg';
      case 8:
        return 'md';
      default:
        return 'sm';
    }
  };

  const renderThumbnail = () => { 
     switch (card.type) {
      case 'video':
        return <img src={card.img.src} alt={card.title} width='100%' height="275" style={{ display: "block" }} />;
      case 'playlist':
        return <img src={card.img.src} alt={card.title} width='100%' height="275" style={{ display: "block" }} />;
      case 'image':
        return <img src={card.img.src} alt={card.title} width='100%' height="275" style={{ display: "block" }} />;
      case 'document':
        return <>Placeholder Document</>;
      case 'chart':
        return <>Placeholder Chart</>;
      case 'search query':
        return <>Placeholder SearchQuery</>;
      default:
        return <>Thumbnail</>;
    }
  }

  return (
    <Grid component="li" item {...size}>
      <button>Drag and drop</button>
      <button>Delete</button>
      <SCard>
        <CardHeader
          avatar={
            <Avatar aria-label="type">
               {getAvatar(card.type)}
            </Avatar>
          }
          title={title}
          titleTypographyProps={{
          variant: 'subtitle2',
          }}
        />
        <CardThumbnail size={getRatio()}>{renderThumbnail()}</CardThumbnail>
        <CardDescription size={getRatio()}>
          {renderCardContent()}
        </CardDescription>
      </SCard>
    </Grid>
  );
};

CardEdit.propTypes = {
  card: shape({
    span: number,
    type: oneOf([
      'image',
      'video',
      'document',
      'playlist',
      'chart',
      'placeholder',
    ]),
    title: string,
    description: string,
  }),
};

CardEdit.defaultProps = {
  card: {
    span: 12,
    type: 'placeholder',
    title: 'Enter your title here',
    description: 'Enter your description here',
  },
};

export default CardEdit;

import React from 'react';
import { element, arrayOf } from 'prop-types';
import { useMode } from '../../contexts/ModeContext';
import { SCards } from './styles';
import CardView from './CardView';
import CardEdit from './CardEdit';

const Cards = ({ children }) => {
  const [mode] = useMode();

  return (
    <SCards container spacing={3} component="ul">
      {React.Children.map(children, (fragment) => {
        return React.Children.map(fragment.props.children, (child) => {
          return typeof child.type === 'string'
            ? child
            : React.cloneElement(child, { mode });
        });
      })}
    </SCards>
  );
};

Cards.propTypes = {
  children: arrayOf(element).isRequired,
};

Cards.CardView = CardView;
Cards.CardEdit = CardEdit;

export default Cards;

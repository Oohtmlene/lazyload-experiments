import styled from 'styled-components';
import { Card as MuiCard, Grid } from '@material-ui/core';

const SCards = styled(Grid)`
  && {
    list-style: none;
    margin-bottom: 12px;
    padding: 0;
  }
`;

const SCard = styled(MuiCard)`
  && {
    height: 100%;
    border: 1px solid #e5e5e6;
    border-radius: 0;
    box-shadow: none;
    background: rgba(255, 255, 255, 0.8);
  }
`;

export { SCards, SCard };

import React from 'react';
import { oneOf, element } from 'prop-types';
import styled, { css } from 'styled-components';

const CardThumbnail = ({ children, size }) => {
  return <SCardThumbnail size={size}>{children}</SCardThumbnail>;
};

// eslint-disable-next-line no-unused-vars
const SCardThumbnail = styled(({ size, ...rest }) => <div {...rest} />)`
  ${({ size }) => {
    if (size === 'md') {
      return css`
        float: right;
        width: 65%;
      `;
    }

    if (size === 'lg') {
      return css`
        position: relative;
        z-index: 1;
        float: right;
        width: 55%;
        &::before {
          position: absolute;
          bottom: 0;
          left: -50px;
          z-index: 0;
          display: block;
          width: 50px;
          height: 90%;
          content: '';
          background: #e0eefa;
        }
      `;
    }
  }}
  border: 1px solid #e5e5e6;
  background: #f4f4f4;
  border-width: 1px 0;
`;

CardThumbnail.propTypes = {
  children: element.isRequired,
  size: oneOf(['sm', 'md', 'lg']),
};

CardThumbnail.defaultProps = {
  size: 'sm',
};

export default CardThumbnail;

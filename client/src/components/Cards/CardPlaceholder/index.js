import React from 'react';
import { number } from 'prop-types';
import styled from 'styled-components';
import { Grid, Card, CardHeader, CardContent } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';


const SCard = styled(Card)`
  && {
    height: 100%;
    border: 1px solid #e5e5e6;
    border-radius: 0;
    box-shadow: none;
    background: rgba(255, 255, 255, 0.8);
  }
`;

const CardPlaceholder = ({ span }) => {

  const size = {
    xs: span,
    sm: span,
    md: span,
    lg: span,
    xl: span,
  };

  return (
    <Grid component="li" item {...size}>
      <SCard>
        <CardHeader
            avatar={<Skeleton animation="wave" variant="circle" width={48} height={48} />}
            title={<Skeleton animation="wave" height={32} width="100%" style={{ marginBottom: 6 }} />}
        />
        <Skeleton animation="wave" variant="rect" height={300} />
        <CardContent>
          <React.Fragment>
              <Skeleton animation="wave" height={24} style={{ marginBottom: 6 }} />
              <Skeleton animation="wave" height={24} width="80%" />
            </React.Fragment>
        </CardContent>
      </SCard>
    </Grid>
  )
}

CardPlaceholder.propTypes = {
  span: number,
};

CardPlaceholder.defaultProps = {
  span: 12,
};

export default CardPlaceholder;

import React from 'react';
import { oneOf } from 'prop-types';
import { usePreview } from '../../contexts/PreviewContext';

function Preview() {
  const [preview] = usePreview();

  return (
    <video
      width="100%"
      src={preview?.data?.src}
      poster={preview?.data?.poster}
      playsInline
      loop
      muted
      autoPlay
    >
      {/* <source data-src="one-does-not-simply.webm" type="video/webm"/> */}
      <source data-src={preview?.data?.src} type="video/mp4" />
      Your browser does not support the video tag.
    </video>
  );

  /* Simple video
     <video
        width="100%"
        src={preview?.el?.src}
        poster={preview?.el?.poster}
        playsInline
        loop
        muted
        autoPlay
      >
        <source data-src="one-does-not-simply.webm" type="video/webm"/>
        <source data-src={preview?.el?.src} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
  */
  /* Dialog video
    <>
      Title
     <video
        width="100%"
        src={preview?.el?.src}
        poster={preview?.el?.poster}
        playsInline
        loop
        muted
        autoPlay
      >
        <source data-src="one-does-not-simply.webm" type="video/webm"/>
        <source data-src={preview?.el?.src} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
      Description
    </>
  */
}

Preview.propTypes = {
  type: oneOf(['video', 'img', 'doc', 'chart']),
  layout: oneOf(['default', 'dialog']),
};

Preview.defaultProps = {
  type: 'video',
  layout: 'default',
};

export default Preview;

import React from 'react';
import { shape, string } from 'prop-types';

import PreviewTrigger from '../VideoPreviewTrigger';
import RenderPreview from './RenderPreview';

const VideoPreview = ({ item }) => {
  return (
    <PreviewTrigger event="hover" data={item} component={<RenderPreview />}>
      <img
        width="100%"
        height="auto"
        alt={item.title}
        src={item.img.src}
        data-src={item.img['data-src']}
        style={{
          display: 'block',
          objectFit: 'cover',
        }}
      />
    </PreviewTrigger>
  );
};

VideoPreview.propTypes = {
  item: shape({
    id: string,
    title: string,
    img: shape({
      src: string,
      'data-src': string,
    }),
  }),
};

VideoPreview.defaultProps = {
  item: {
    id: null,
    src: '',
    title: '',
    img: {
      src: '',
      'data-src': '',
    },
  },
};

export default VideoPreview;

import React from 'react';
import styled, { css } from 'styled-components';
import { usePreview } from '../../contexts/PreviewContext';

// **
// ** Get Bounding box
// **

const SPreview = styled.div`
  position: absolute;
  z-index: 2;
  top: ${(props) => props.boundingBox.top}px;
  left: ${(props) => props.boundingBox.left}px;
  width: ${(props) => props.boundingBox.width}px;
  height: ${(props) => props.boundingBox.height}px;
  transition: opacity 100ms ease-out;
  ${({ eventType }) => {
    if (eventType === 'mouseenter')
      return css`
        opacity: 0;
        &:hover {
          opacity: 1;
        }
      `;
  }}
`;

function getBoundingBoxPage(elem) {
  let box = elem.getBoundingClientRect();

  return {
    top: box.top + window.pageYOffset,
    right: box.right + window.pageXOffset,
    bottom: box.bottom + window.pageYOffset,
    left: box.left + window.pageXOffset,
    width: box.width,
    height: box.height,
    x: box.x,
    y: box.y,
  };
}

function PreviewHolder() {
  const [preview, setPreview] = usePreview();
  if (!preview.show) return <></>;

  const boundingBox = getBoundingBoxPage(preview.trigger.target);

  const handleMouseLeave = () => {
    setPreview((prevPreview) => ({ ...prevPreview, show: false }));
  };

  const eventType = preview.trigger.type;
  const events = {
    ...(eventType === 'mouseenter' ? { onMouseLeave: handleMouseLeave } : {}),
  };

  return (
    <SPreview eventType={eventType} {...events} boundingBox={boundingBox}>
      {preview.component}
    </SPreview>
  );
}

export default PreviewHolder;

import React from 'react';
import { string, shape } from 'prop-types';
import { useLazyQuery } from '@apollo/client';
import useInView from "react-cool-inview";
import WIDGET from '../../api/queries/widget';
import { useMode } from '../../contexts/ModeContext';
import { SCards } from './styles';
import CardEdit from "../Cards/CardEdit";
import CardView from "../Cards/CardView";
import CardPlaceholder from "../Cards/CardPlaceholder";

const Cards = ({ widget }) => {
  const [mode] = useMode();
  // fetch data here or in card?
  const [getItems, { data }] = useLazyQuery(WIDGET, {
    variables: {
      uid: widget.uid
    }
  });

  const { observe } = useInView({
    // For better UX, we can grow the root margin so the data will be loaded earlier
    rootMargin: "50px 0px",
    // When the last item comes to the viewport
    onEnter: ({ unobserve, observe }) => {
      // Pause observe when loading data
      unobserve();
      // Load more data
      getItems();
      observe();
    },
  });

  const isEditMode = mode === "edit";

  const renderCards = () => {
    if (data) return data.widget.items.map(card => {
      return isEditMode ? <CardEdit key={card.uid} card={card} /> : <CardView key={card.uid} card={card} />
    })

    return widget.items.map(card => {
      return <CardPlaceholder key={card.uid} span={card.span} />
    })
  };

  return (
    <>
      <SCards ref={observe} container spacing={3} component="ul">{renderCards()}</SCards>
    </>
  )
}


Cards.propTypes = {
  widget: shape({
    uid: string.isRequired,
    type: string.isRequired,
  }),
};


export default Cards;

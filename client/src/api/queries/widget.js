import { gql } from '@apollo/client';

const WIDGET = gql`
  query Widget($uid: ID!) {
    widget(uid: $uid) {
      position
      uid
      type
      items {
        uid
        type
        span
        title
        description
        src
        poster
        img {
          src
          dataSrc
          dataSrcset
          srcset
        }
      } 
    }
  }
`;

export default WIDGET;

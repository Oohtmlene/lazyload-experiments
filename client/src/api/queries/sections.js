import { gql } from '@apollo/client';

const SECTIONS = gql`
  query Sections {
    sections {
      position
      uid
      widgets {
        position
        uid
        type
        items {
          uid
          position
          span
        }
      }
    }
  }
`;

export default SECTIONS;
